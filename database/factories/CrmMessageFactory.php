<?php

use Faker\Generator as Faker;

$factory->define(App\CrmMessage::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'message' => $faker->realText($maxNbChars = 60),
    ];
});
