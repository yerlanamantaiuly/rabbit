<?php

use Faker\Generator as Faker;

$factory->define(App\Email::class, function (Faker $faker) {
    return [
        'to' => $faker->email,
        'from' => $faker->companyEmail,
        'subject' => $faker->name,
        'message' => $faker->realText($maxNbChars = 60),
    ];
});
