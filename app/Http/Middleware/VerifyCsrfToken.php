<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    public function handle($request, \Closure $next)
    {
        if (in_array(env('APP_ENV'), ['local', 'dev'])) {
            return $next($request);
        }

        return parent::handle($request, $next);
    }

    protected $except = [
        //
    ];
}
