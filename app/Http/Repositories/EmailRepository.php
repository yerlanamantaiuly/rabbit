<?php

namespace App\Repositories;

use App\Email;
use App\Enums\QueueName;
use App\Jobs\SendEmail;

class EmailRepository
{

    public function store(array $data)
    {
        return Email::create($data);
    }

    public function dispatch(Email $email) {
        dispatch(new SendEmail($email))->onQueue(QueueName::Q_EMAIL);
    }

}