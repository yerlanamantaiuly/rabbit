<?php

namespace App\Repositories;

use App\Enums\QueueName;
use Illuminate\Support\Facades\Queue;

class QueueRepository
{

    public function sendBroadcast($name,$type,$message)
    {
        $collection = collect(
            [
                'name' => $name,
                'type' => $type,
                'message' => $message
            ]
        );

        //Отпраляем JSON в очередь для нашего сервиса
        Queue::pushRaw($collection->toJson(), QueueName::Q_BROADCAST);
    }



}