<?php

namespace App\Repositories;

use App\CrmMessage;
use App\Email;
use App\Enums\QueueName;
use App\Jobs\SendCrmMessage;

class CrmMessageRepository
{
    public function store(array $data)
    {
        return CrmMessage::create($data);
    }

    public function dispatch(CrmMessage $message) {
        dispatch(new SendCrmMessage($message))->onQueue(QueueName::Q_CRM_MESSAGE);
    }

}