<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\CrmMessageFormRequest;
use App\Repositories\CrmMessageRepository;

class CrmMessageController extends ApiBaseController
{
    public function store(CrmMessageFormRequest $request, CrmMessageRepository $crmMessageRepository) {
        $data = $request->only(['title','message']);

        //Создаем задачу в БД
        $model = $crmMessageRepository->store($data);

        //Отправляем в очередь
        $crmMessageRepository->dispatch($model);

        return $this->sendResponse(
            true, ['status'=>'success']
        );
    }
}
