<?php

namespace App\Http\Controllers;

class ApiBaseController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($success, $result, $code = 200)
    {
        $response = [
            'success'   => $success,
        ];

        return response()->json(array_merge($response, ['result'=>$result]), $code);
    }

}

