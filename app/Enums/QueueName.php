<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class QueueName extends Enum
{
    const Q_EMAIL = 'email';
    const Q_CRM_MESSAGE = 'crm_message';
    const Q_BROADCAST = 'broadcast';
}
