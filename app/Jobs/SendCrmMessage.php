<?php

namespace App\Jobs;

use App\CrmMessage;
use App\Repositories\QueueRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendCrmMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $message;

    public function __construct(CrmMessage $message)
    {
        $this->message = $message;
    }

    public function handle(QueueRepository $queueRepository)
    {
        //Представим что тут идет отправка по API в нашу CRM
        $this->message->job_status = true;
        $this->message->save();

        //Отправляем сообщение что выполнили задачу в Вебсокет сервис через очередь
        $queueRepository->sendBroadcast($this->message->title,'crm',$this->message->message);
    }
}
