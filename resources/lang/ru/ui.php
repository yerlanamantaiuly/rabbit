<?php

return [

    'registration_title' => 'Регистрация',
    'signin_title' => 'Вход',
    'login_registration_text' => 'Регистрация',
    'login_recovery_password_text' => 'Восстановление пароля',
    'password' => 'Пароль',
    'reset_password_title' => 'Восстановление пароля',
    'reset_password_content_main' => "Вы воспользовались функцией восстановления пароля на сервисе \":site_name\".<br><br>Если это были не Вы — просто проигнорируйте это письмо.<br><br>Для восстановления пароля нажмите на кнопку:",
    'reset_password_content_link' => 'Или перейдите по ссылке:',
    'logout' => 'Выход',
    'location' => 'Локация',
    'duration' => 'Продолжительность',
    'category' => 'Категория',
    'all' => 'Все',
    'days' => '{0} дней|{1} день|[2,4] дня|[5,999999] дней',
    'duration' => 'продолжительность',
    'price' => 'Стоимомть участия',
    'KZT' => 'тг',
    'not_register_question' => 'Не зарегистрированы на сайте?',
    'login_and_win' => 'Заходите, участвуйте и побеждайте',

    'reset_password_text' => 'Ссылка для восстановления пароля будет отправлена вам на почту',
    'are_you_register' => 'Уже зарегистрированы?',
    'privacy_policy' => 'Политика конфединциальности',
    'send_reset_link' => 'Отправить',
    'register' => 'Зарегистрируйтесь',
];
