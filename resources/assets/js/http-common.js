import axios from 'axios'

export const HTTP = axios.create({
  headers: {
    'content-type': 'application/x-www-form-urlencoded',
    'X-Requested-With': 'XMLHttpRequest'
  }
})
